call plug#begin('~/.vim/plugged')

" ======================Plugins=================================
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'digitaltoad/vim-pug'
Plug 'flowtype/vim-flow'
Plug 'dag/vim-fish'
Plug 'dense-analysis/ale'
Plug 'pangloss/vim-javascript'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mattn/emmet-vim'
Plug 'leafgarland/typescript-vim'
Plug 'ianks/vim-tsx'
Plug 'jxnblk/vim-mdx-js'
Plug 'evanleck/vim-svelte'
" Plug 'peitalin/vim-jsx-typescript'
Plug 'dart-lang/dart-vim-plugin'
Plug 'udalov/kotlin-vim'
Plug 'othree/html5.vim'
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-fireplace'
Plug 'tpope/vim-salve'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }
Plug 'Vigemus/iron.nvim'
Plug 'ap/vim-css-color'
Plug 'tpope/vim-commentary'
Plug 'jlanzarotta/bufexplorer'
Plug 'thiagoalessio/rainbow_levels.vim'
Plug 'wakatime/vim-wakatime'
Plug 'tweekmonster/django-plus.vim'
"Plug 'terryma/vim-multiple-cursors'
Plug 'terryma/vim-expand-region'
Plug 'mhinz/vim-grepper', { 'on': ['Grepper', '<plug>(GrepperOperator)'] }
"Plug 'mustache/vim-mustache-handlebars'
"Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
" Fuzzy finder
Plug 'airblade/vim-rooter'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'stamblerre/gocode', { 'rtp': 'vim', 'do': '~/.vim/plugged/gocode/vim/symlink.sh' }
"Plug 'mdempsky/gocode', { 'rtp': 'vim', 'do': '~/.vim/plugged/gocode/vim/symlink.sh' }
Plug 'ryanoasis/vim-devicons'

"COLORSCHEMES

Plug 'rakr/vim-one'
Plug 'vim-scripts/Visual-Studio'
Plug 'doums/darcula'
Plug 'shapeoflambda/dark-purple.vim'
Plug 'mushanyoung/vim-windflower'
Plug 'jaredgorski/spacecamp'
Plug 'junegunn/seoul256.vim'
Plug 'fenetikm/falcon'
Plug 'phanviet/vim-monokai-pro'
Plug 'chriskempson/base16-vim'
Plug 'morhetz/gruvbox'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'NLKNguyen/papercolor-theme'
Plug 'kabbamine/yowish.vim'
Plug 'lifepillar/vim-solarized8'
Plug 'ntk148v/vim-horizon' 
Plug 'challenger-deep-theme/vim'
Plug 'herrbischoff/cobalt2.vim'
Plug 'lithammer/vim-eighties'
Plug 'mhartington/oceanic-next'
Plug 'flrnd/plastic.vim'

" Initialize plugin system
call plug#end()

" lua
luafile $HOME/.config/nvim/plugins.lua

" ===========================================================
set termguicolors
" let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
" let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
" let g:seoul256_background = 238
set background=light
" colorscheme one
colorscheme horizon
" colorscheme plastic
highlight Normal cterm=NONE term=NONE
highlight Normal ctermbg=NONE
highlight nonText ctermbg=NONE

" SETTINGS

set t_Co=256
set backupcopy=yes

" GLOBALS
let g:jsx_ext_required = 0
let g:vim_jsx_pretty_highlight_close_tag = 1
let g:typescript_indent_disable = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ale#enabled = 1
" let g:airline_theme = 'one'
let g:airline_theme='fruit_punch'
" let g:airline_theme='sol'
let g:airline_powerline_fonts = 1


autocmd BufRead,BufNewFile *.hbs set filetype=html
autocmd BufRead,BufNewFile *.js.flow set filetype=javascript
" autocmd BufRead,BufNewFile *.tsx set filetype=typescript
augroup web
    autocmd BufReadPre *.js set tabstop=2
    autocmd BufReadPre *.js set shiftwidth=2
    autocmd BufReadPre *.jsx set tabstop=2
    autocmd BufReadPre *.jsx set shiftwidth=2
    autocmd BufReadPre *.ts set tabstop=2
    autocmd BufReadPre *.ts set shiftwidth=2
    autocmd BufReadPre *.tsx set tabstop=2
    autocmd BufReadPre *.tsx set shiftwidth=2
    autocmd BufReadPre *.dart set tabstop=2
    autocmd BufReadPre *.dart set shiftwidth=2
    autocmd BufReadPre *.json set tabstop=2
    autocmd BufReadPre *.json set shiftwidth=2
    autocmd BufReadPre *.scss set tabstop=2
    autocmd BufReadPre *.scss set shiftwidth=2
    autocmd BufReadPre *.css set tabstop=2
    autocmd BufReadPre *.css set shiftwidth=2
augroup END

augroup config
    autocmd BufReadPre *.json set tabstop=2
    autocmd BufReadPre *.json set shiftwidth=2
    autocmd BufReadPre *.yml set tabstop=2
    autocmd BufReadPre *.yml set shiftwidth=2
    autocmd BufReadPre *.toml set tabstop=2
    autocmd BufReadPre *.toml set shiftwidth=2

augroup html
    autocmd BufReadPre *.pug set tabstop=4
    autocmd BufReadPre *.pug set shiftwidth=4
    autocmd BufReadPre *.html set tabstop=4
    autocmd BufReadPre *.html set shiftwidth=4
    autocmd BufReadPre *.hbs set tabstop=4
    autocmd BufReadPre *.hbs set shiftwidth=4
augroup END

augroup clike
    autocmd BufReadPre *.c set tabstop=4
    autocmd BufReadPre *.c set shiftwidth=4
    autocmd BufReadPre *.cpp set tabstop=4
    autocmd BufReadPre *.cpp set shiftwidth=4
    autocmd BufReadPre *.h set tabstop=4
    autocmd BufReadPre *.h set shiftwidth=4
    autocmd BufReadPre *.hpp set tabstop=4
    autocmd BufReadPre *.hpp set shiftwidth=4
    autocmd BufReadPre *.rs set tabstop=4
    autocmd BufReadPre *.rs set shiftwidth=4
    autocmd BufReadPre *.go set tabstop=4
    autocmd BufReadPre *.go set shiftwidth=4
augroup END

set number
set expandtab
set tabstop=4
set shiftwidth=4
set hlsearch
set incsearch
syntax on
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=2

" fix pop menu background color
hi Pmenu ctermbg=7 ctermfg=20 guifg=#d5d8da guibg=#585858


let g:terminal_color_0 = '#1c1e26'
  let g:terminal_color_1 = '#eC6a88'
  let g:terminal_color_2 = '#09f7a0'
  let g:terminal_color_3 = '#fab795'
  let g:terminal_color_4 = '#25b0bc'
  let g:terminal_color_5 = '#f09483'
  let g:terminal_color_6 = '#e95678'
  let g:terminal_color_7 = '#1c1e26'
  let g:terminal_color_8 = '#d5d8da'
  let g:terminal_color_9 = '#ec6a88'
  let g:terminal_color_10 = '#6bdfe6'
  let g:terminal_color_11 = '#fab38e'
  let g:terminal_color_12 = '#21bfc2'
  let g:terminal_color_13 = '#b877db'
  let g:terminal_color_14 = '#95c4ce'
  let g:terminal_color_15 = '#d2d4de'

"============mappings=================

" indent shifts keep selection 
vnoremap < <gv
vnoremap > >gv
map <C-n> :NERDTreeToggle<CR>
map <C-M-n> :NERDTreeFocus<CR>
inoremap jk <Esc>`^
map <C-p> :FZF<CR>

" To save, ctrl-s.
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a


let g:AutoPairsShortcutFastWrap='<C-e>'
let g:go_gocode_propose_source = 1
xnoremap @ :<C-u>call ExecuteMacroOverVisualRange()<CR>

function! ExecuteMacroOverVisualRange()
    echo "@".getcmdline()
    execute ":'<,'>normal @".nr2char(getchar())
endfunction

set conceallevel=0
let g:vim_json_syntax_conceal = 0
"emmet
"let g:user_emmet_leader_key='<C-d>'
"ale
let g:ale_linters = {'javascript': ['eslint'],
            \    'typescript': ['eslint']
            \}
let g:ale_fixers = {
            \   'rust': ['rustfmt'],
            \   'javascript': ['prettier', 'eslint'],
            \   'javascriptreact': ['prettier', 'eslint'],
            \   'typescript': ['prettier', 'eslint'],
            \   'typescriptreact': ['prettier', 'eslint'],
            \   'css': ['prettier'],
            \   'scss': ['prettier'],
            \   'sass': ['prettier'],
            \   'go': ['gofmt'],
            \   'ruby': ['rubocop'],
            \   'dart': ['dartfmt'],
            \   'kotlin': ['ktlint'],
            \   'json': ['prettier', 'eslint'],
            \   'html': ['prettier']
            \}

"=======FUNCTIONS=====
"

command! Todo :Grepper -tool rg -query TODO\|FIXME

" function! s:check_back_space() abort "{{{
"     let col = col('.') - 1
"     return !col || getline('.')[col - 1]  =~ '\s'
" endfunction"}}}
" inoremap <silent><expr> <TAB>
"             \ pumvisible() ? "\<C-n>" :
"             \ <SID>check_back_space() ? "\<TAB>" :
"             \ deoplete#manual_complete()

"=================languageClient-nvim=================
" Required for operations modifying multiple buffers like rename.
" set hidden

" let g:LanguageClient_serverCommands = {
"             \ 'rust': ['rustup', 'run', 'stable', 'rls'],
"             \ 'typescript': ['/home/radicalpacifist/.yarn/bin/typescript-language-server', '--stdio'],
"             \ 'javascript': ['/home/radicalpacifist/.yarn/bin/typescript-language-server', '--stdio'],
"             \ 'typescript.tsx': ['/home/radicalpacifist/.yarn/bin/typescript-language-server', '--stdio'],
"             \ 'javascript.jsx': ['/home/radicalpacifist/.yarn/bin/typescript-language-server', '--stdio'],
"             \ 'css' : ['css-languageserver', '--stdio'],
"             \ 'scss' : ['css-languageserver', '--stdio'],
"             \ 'sass' : ['css-languageserver', '--stdio'],
"             \ 'ruby' : ['solargraph', 'stdio'],
"             \ 'go' : ['gopls']
"             \ }

" let g:LanguageClient_rootMarkers = {
"     \ 'javascript': ['jsconfig.json'],
"     \ 'typescript': ['tsconfig.json'],
"     \ }

" nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
" nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
" nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

"=========coc-configuration=============================
"
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"


inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
set updatetime=300
set cmdheight=1
set hidden
" Use `[c` and `]c` for navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K for show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction
" function g:PrettierFormat()
"     call CocAction('runCommand', 'prettier.formatFile')
"     noremap u i
" endfunction
command! -nargs=0 Prettier :call CocAction('runCommand', 'prettier.formatFile')
"Prettier
vmap <leader>f  <Plug>(coc-format-selected)
