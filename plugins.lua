local iron = require('iron')

iron.core.add_repl_definitions {
    python = {
        ipython = {
            command = {"ipython"}
        }
    },

    ruby = {
        irb = {
            command = {"irb"}
        }
    }
}

iron.core.set_config {
    preferred = {
        python = "ipython",
        ruby = "irb"
    }
}
